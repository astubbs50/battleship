var MultiPlayerServer = (function ($) {
	var gameRoom;
	var playerId;
	var playerBeginTurn;
	var playerGetMove;
	var playerGameOver;
	var is_waiting;
	var wait_message = "";
	var wait_message_shown = false;
	
	return {
		stop: function () {
			is_waiting = false;
			hideWaitMessage();
		},
		create: function (callback) {
			var data = {
				operation: "create"
			};
			send_json(data, function (res) {
				gameRoom = res.id;
				playerId = res.playerId;
				gameState.gameRoom = gameRoom;
				gameState.playerId = playerId;
				updateState();
				if(typeof callback === "function") {
					//Return the game room
					callback(gameRoom);
				}
			});
		},
		load: function (_gameRoom, callback) {
			gameRoom = _gameRoom;
			var data = {
				operation: "load",
				gameRoom: gameRoom
			};
			send_json(data, function (res) {				
				playerId = res;
				gameState.gameRoom = gameRoom;
				gameState.playerId = playerId;
				updateState();
				if(typeof callback === "function") {
					callback();
				}
			});
		},
		begin: function (myBoard, myBeginTurn, myGetMove, myGameOver, loadGame) {	
			//console.log(myBoard);
			
			//if game is being loaded from local storage
			if(loadGame) {
				gameRoom = gameState.gameRoom;
				playerId = gameState.playerId;	
				is_waiting = true;
				showWaitMessage();
				setTimeout(getNextMove, 1000);
			} else {
				var data = {
					operation: "begin",
					gameRoom: gameRoom,
					playerId: playerId,
					board: myBoard
				};
				send_json(data, function (res) {
					if(res === "success") {
						is_waiting = true;
						showWaitMessage();
						setTimeout(getNextMove, 1000);
					}
					//console.log(data);
				});
			}
			
			//Store callbacks
			playerBeginTurn = myBeginTurn;
			playerGetMove = myGetMove;
			playerGameOver = myGameOver;			
		},
		endTurn: function (x, y, callback) {
			var data = {
				operation: "end_turn",
				gameRoom: gameRoom,
				playerId: playerId,
				x: x,
				y: y
			};
			send_json(data, function (res) {				
				//callback({shot: "hit", sunk: ship.name});				
				callback(res);				
				is_waiting = true;
				if(res.shot === "miss") {
					showWaitMessage();
				}
				setTimeout(getNextMove, 1000);
			});
		}, 
		loadRoom: function () {
			gameRoom = gameState.gameRoom;
			playerId = gameState.playerId;	
		}
	};
	
	function showWaitMessage() {
		if(!wait_message_shown) {
			wait_message_shown = true;
			setTimeout(function () {				
				$("#msg2").css("display", "block");
				setTimeout(function () { 
					$("#msg2").css("opacity", 1);
				}, 10);
			}, 1000);
		}
		wait_message += ".";
		if(wait_message.length > 3) {
			wait_message = "";
		}
		$("#msg2").html("Waiting for other player" + wait_message);
	}
	
	function hideWaitMessage() {
		if(wait_message_shown) {
			wait_message_shown = false;
			$("#msg2").css("opacity", 0);
			setTimeout(function () { 
				$("#msg2").css("display", "none");
			}, 1000);
		}
	}
	
	function getNextMove() {		
		if(is_waiting) {	
			showWaitMessage();
			var data = {
				operation: "get_move",
				gameRoom: gameRoom,
				playerId: playerId
			};
			send_json(data, function (res) {
				if(res.turn) {
					is_waiting = false;
					hideWaitMessage();
					setTimeout(playerBeginTurn, 100);
				} 
				
				if(res.game_over) {
					is_waiting = false;
					hideWaitMessage();
					setTimeout(function () {
						if(res.game_over === playerId) {
							playerGameOver("computer");
						} else {
							playerGameOver("player");
						}
					}, 100);					
				} 
				
				if(res.shot) {
					//{shot: "hit", x: x, y: y, sunk: false};
					var callback_data = {
						shot: res.shot.shot,
						x: Number(res.shot.x),
						y: Number(res.shot.y),
						sunk: res.shot.sunk
					};
					playerGetMove(callback_data);
					//playerGetMove(res.shot);
				}
			});
			if(is_waiting) {
				setTimeout(getNextMove, 1000);
			} else {
				hideWaitMessage();
			}
		} 
	}
	
	function send_json(data, callback) {
		$.ajax({
			type: "POST",
			url: "battleship_server.php",
			data: data,
			dataType: "json",
			success: function (data) {
				callback(data);
			}
		});	
	}
})(jQuery);