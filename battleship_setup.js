var Server;
var gameState = {};	
var boardSize = 10;
function updateState() {
	if(localStorage) {
		if(gameState.gameOver) {
			localStorage.removeItem("game-state");
			$("#btnOption2").prop("disabled", true);
		} else {
			localStorage["game-state"] = JSON.stringify(gameState);
		}
	}
}

(function ($) {
	var myBoard;
	var btnRotateDims = {};
	var myScreenTransformDims = {};
	var otherScreenTransformDims = {};
	var transformed = false;
	var $cellPlacement = false;
	var $ship = false;
	var $selected_ship = false;
	var ship_animating = false;
	var boards_created = false;
	var lx, ly;
	var ship_data = {
		segments: 0,
		offsetY: 0,
		offsetX: 0
	};	
	var eventsEnabled = false;
	var multiplayer_game = getParameterByName("g");
	
	if(localStorage) {
		if(localStorage["game-state"]) {
			gameState = JSON.parse(localStorage["game-state"]);
		} else {
			gameState.status = "new game";
			gameState.shipState = {};
			$("#btnOption2").prop("disabled", true);
		}		
	}
	$(document).ready(function () {
		var $btnOne = $("#btnOption1");
		var $btnTwo = $("#btnOption2");
		var $btnLinkOk = $("#btnLinkOk");
		
		if(multiplayer_game) {
			$("#divStartGame h1").html("Battleship - Multiplayer");
			$btnOne.val("Start Game");
			$btnTwo.val("Cancel");
			$("#btnOption2").prop("disabled", false);
		}
		$btnOne.on("click", function () {
			//startSetup();
			if($btnOne.val() === "New Game") {
				animateButtonChange($("#btnOption1, #btnOption2"), 250, false, function () {
					$("#btnOption2").prop("disabled", true);
					$btnOne.val("Single Player");
					$btnTwo.val("Multiplayer");

					animateButtonChange($("#btnOption1, #btnOption2"), 250, true, false);
				});
			} else if($btnOne.val() === "Single Player") {
				Server = SinglePlayerServer;
				gameState.gameType = "single player";
				hideMenu();
				startNewGame();
			} else if($btnOne.val() === "Start Game") {
				Server = MultiPlayerServer;
				gameState.gameType = "multiplayer";
				Server.load(multiplayer_game, function () {
					hideMenu();
					startNewGame();
				});
			}
		});
		
		$btnTwo.on("click", function () { 
			if($btnTwo.val() === "Continue") {	
				if(gameState.gameType === "multiplayer") {
					Server = MultiPlayerServer;
					Server.loadRoom();
				} else {
					Server = SinglePlayerServer;
				}
				hideMenu();
				continueGame();
			} else if ( $btnTwo.val() === "Multiplayer" ) {
				Server = MultiPlayerServer;
				gameState.gameType = "multiplayer";
				$("#txtMulitiplayerLink").val("Getting game link...");
				Server.create(function (gameRoom) { 
					var base_link = location.protocol + '//' + location.host + location.pathname;
					var link = base_link + "?g=" + gameRoom;
					$("#txtMulitiplayerLink").val(link);
				});
				
				$("#divOptionButtons").fadeOut(250, function () {
					$("#divMultiplayerLink").fadeIn(250);
				});				
			} else if ( $btnTwo.val() === "Cancel" ) {
				animateButtonChange($("#divStartGame h1, #btnOption1, #btnOption2"), 250, false, function () {
					$("#divStartGame h1").html("Battleship");
					$btnOne.val("New Game");
					$btnTwo.val("Continue");
					animateButtonChange($("#divStartGame h1, #btnOption1, #btnOption2"), 250, true, false);
					if(gameState.status === "new game") {
						$("#btnOption2").prop("disabled", true);
					}
				});
			}
		});
		
		$btnLinkOk.on("click", function () { 
			hideMenu();			
			startNewGame();
		});
	});
	
	function animateButtonChange($btn, duration, reverse, callback) {
		var start = (new Date()).getTime();
		animateStep();
		function animateStep() {
			var dt = (new Date).getTime() - start;
			var alpha = Math.sin((dt / duration) * Math.PI * 0.5);
			if(!reverse) {
				alpha = 1 - alpha;
			}
			$btn
				.css("color", "rgba(255, 255, 255, " + alpha + ")")
				.css("text-shadow",  "-1px 1px rgba(0, 0, 0, " + alpha + ")");
			if(dt < duration) {
				requestAnimationFrame(animateStep);
			} else {
				if(callback) {
					callback();
				}
			}
		}
	}
	
	function hideMenu() {	
		BattleShip.disableEvents();
		$("#divStartOverlay").animate({
			opacity: 0
		}, 1000, function () { 
			$(this).css("display", "none");
		});
		$("#divStartGame").animate({			
			left: 0,
			top: 0,
			width: 50,
			height: 50
		}, 1000, function () {
			setTimeout(function () { 
				$("#divOptions").on("click", showMenu);
			}, 100);
		});
		
		$("#divStartGame .btn, #divStartGame h1").animate({
			opacity: 0
		}, 250, function () {
			$(this).hide();			
			$("#divOptions")
				.css("display", "block")
				.animate({ opacity: 1}, 250);
		});
	}
	
	function showMenu() {
		$("#msg").hide();
		$("#btnOption1").val("New Game");
		$("#btnOption2").val("Continue");
		$("#divOptions").off("click", showMenu);	
		Server.stop();
		
		//Animate Options Button
		$("#divOptions").animate({ opacity: 0}, 750, function () {
			$(this).css("display", "none");
			
			$("#divStartGame .btn, #divStartGame h1")
				.css("display", "block")
				.animate({
					opacity: 1
				}, 250);
		});
		
		//Animate Overlay
		$("#divStartOverlay").css("display", "block");
		$("#divStartOverlay").animate({
			opacity: 1
		}, 1000, function () { 
			
		});
		
		//Animate Options Menu
		var left = $(window).width() / 2 - 250;
		var top = $(window).height() / 2 - 125;
		$("#divStartGame").animate({			
			left: left,
			top: top,
			width: 500,
			height: 150
		}, 1000, function () {
			$(this)
				.css("left", "calc(50% - 250px)")
				.css("top", "calc(50% - 125px)");
		});
	}
	
	function continueGame() {
		myBoard = gameState.board;
		resetBoards();		
		createBoards();
		var ship_cells_places = {};
		for(var i = 0; i < myBoard.length; i++) {
			for(var j = 0; j < myBoard[i].length; j++) {
				for(var k = 0; k < myBoard[i][j].length; k++) {
					var x = i + 1;
					var y = j + 1;
					var $cell = $("#myBoard .cell_" + x + "-" + y);
					var board_cell = myBoard[i][j][k];
					if(!ship_cells_places[board_cell.name]) {
						ship_cells_places[board_cell.name] = $cell;
					}
					ship_cells_places[board_cell.name] = ship_cells_places[board_cell.name].add($cell);
					setCellColor($cell);
				}
			}
		}
		for(var ship_name in gameState.shipState) {
			var $cellPlacement = $("#myBoard .cell_" + gameState.shipState[ship_name].x + "-" + gameState.shipState[ship_name].y);
			var $ship_old = $("#myPieces ." + ship_name);
			var $ship_new = $ship_old.clone();
			
			$ship_old.css("visibility", "hidden");
			
			$ship_new.data("ship", { name: $ship_old.data("shipname"), segments: $ship_old.data("segments"), hits: 0 });
			$ship_new.removeClass("placeholder");
			$ship_new.data("orientation", "vertical");
			$ship_new.data("placed-cells", ship_cells_places[ship_name]);
			$ship_new.data("$cellPlacement", $cellPlacement);				
			$(document.body).append($ship_new);
			var offset = $cellPlacement.offset();
			$ship_new
				.css("position", "absolute")
				.css("left", offset.left)
				.css("top", offset.top);
				
			var $btn = $("<input class='btn2 btn-rotate' type='button' value='' />");	
			$btn
				.css("width", btnRotateDims.width)
				.css("height", btnRotateDims.height)
				.css("margin-left", btnRotateDims.marginLeft + "px")
				.css("background-size", btnRotateDims.width + "px " + btnRotateDims.height + "px");
			$btn.on("click", rotateShip);
			$ship_new.append($btn);	
			placeButton($ship_new);
			if(gameState.shipState[ship_name].orientation === "horizontal") {
				$btn.trigger("click");
			}
		}
		if(gameState.status === "playing") {
			startGame("0s");
		}
		
		setupGameBoard();
	}
	
	function startNewGame() {
        gameState = {};
		gameState.status = "new game";
		gameState.shipState = {};
		gameState.status = "setup";
        gameState.gameOver = false;
		resetBoards();
		myBoard = [];
		for(var x = 0; x < boardSize; x++) {				
			myBoard.push([]);
			for(var y = 0; y < boardSize; y++) {
				myBoard[x].push([]);
			}
		}
		gameState.board = myBoard;
		createBoards();
		setupGameBoard();
	}
	
	function scaleBoard() {
		var maxHeight = $(window).height() * 0.65876;
		var maxWidth = $(window).width() * 0.7;
		var size;
		if(maxWidth < maxHeight) {
			size = maxWidth;
		} else {
			size = maxHeight;
		}
		$(".board")
			.width(size)
			.height(size);
		
		var fontSize = (size * 0.06).toFixed(3);
		$(".board td, .board th")
			.css("font-size", fontSize + "px");
			
		var missileWidth = (size * 0.048).toFixed(3);
		var missileHeight = (size * 0.14).toFixed(3);
		$(".missile")
			.width(missileWidth)
			.height(missileHeight);
				
		var piecesWidth = (size * 0.35).toFixed(3);
		var piecesHeight = (size * 0.76).toFixed(3);
		var piecesMarginRight = (size * 0.02).toFixed(3);
		$(".pieces")
			.width(piecesWidth)
			.height(piecesHeight)
			.css("margin-right", piecesMarginRight + "px");
			
		var shipWidth = (size * 0.0912).toFixed(3);
		
		function setShipSize($ship, shipHeight) {
			$ship.each(function () {
				$this = $(this);
				if($this.data("orientation") && $this.data("orientation") === "horizontal") {
					$this
						.width(shipHeight)
						.height(shipWidth)
						.css("background-size", shipHeight + "px " + shipWidth + "px");
				} else {					
					$this
						.width(shipWidth)
						.height(shipHeight)
						.css("background-size", shipWidth + "px " + shipHeight + "px");
				}
				
				if($this.data("$cellPlacement")) {
					if(transformed) {
						var boardOffset = $("#myScreen").offset();
						var offset = $this.data("$cellPlacement").offset();
						$this.css("left", offset.left - boardOffset.left);
						$this.css("top", offset.top - boardOffset.top);						
					} else {
						var offset = $this.data("$cellPlacement").offset();
						$this.css("left", offset.left);
						$this.css("top", offset.top);
					}
				}
			});
		}
				
		var btnPadWidth = (size * 0.03).toFixed(3);
		var btnPadHeight = (size * 0.046).toFixed(3);
		$(".btn-green, .btn-red")
			.css("padding", btnPadWidth + "px " + btnPadHeight + "px");
			
		var btnFontSize = (size * 0.064).toFixed(3);
		var btnTop = (size * -0.22).toFixed(3);
		var btnLeft = (size * 0.05).toFixed(3);
		$("td > .btn")
			.css("font-size", btnFontSize + "px")
			.css("top", btnTop + "px")
			.css("left", btnLeft + "px");
			
		btnRotateDims.width = (size * 0.064).toFixed(3);
		btnRotateDims.height = (size * 0.068).toFixed(3);
		btnRotateDims.marginLeft = (size * 0.012).toFixed(3);
		$(".btn-rotate")
			.css("width", btnRotateDims.width)
			.css("height", btnRotateDims.height)
			.css("margin-left", btnRotateDims.marginLeft + "px")
			.css("background-size", btnRotateDims.width + "px " + btnRotateDims.height + "px");
						
		//My Screen
		myScreenTransformDims.perspective = (size * 3).toFixed(3);
		myScreenTransformDims.translateY = (size * 0.24).toFixed(3);
		myScreenTransformDims.translateZ = (size * 1.2).toFixed(3);
				
		//Other Screen
		var otherScreenTop = (-size * 1.2).toFixed(3);
		var otherScreenPerspective = (size * 3).toFixed(3);		
		var otherScreenTranslateY = (-size * 1.4).toFixed(3);
		var otherScreenTranslateZ = (size * 0.5).toFixed(3);
		$(".otherScreen")
			.css("transition-duration", "0s")		
			.css("top", otherScreenTop + "px")
			.css("transform", "perspective(" + 
				otherScreenPerspective + "px) translateY(" + 
				otherScreenTranslateY + "px) translateZ(" + 
				otherScreenTranslateZ + "px) rotate3d(1,0,0,-90deg)");
				
		otherScreenTransformDims.perspective = otherScreenPerspective;		
		
		//Reset the transformation before placing the ships
		$("#myScreen")
			.css("transition-duration", "0s")			
			.css("transform", "perspective(" + 
				myScreenTransformDims.perspective + "px) translateY(" + 
				0 + "px) translateZ(" + 
				0 + "px) rotate3d(1,0,0,0deg)");
		
		setShipSize($(".carrier"), (size * 0.448).toFixed(3));		
		setShipSize($(".battleship"), (size * 0.358).toFixed(3));		
		setShipSize($(".cruiser"), (size * 0.27).toFixed(3));		
		setShipSize($(".submarine"), (size * 0.27).toFixed(3));		
		setShipSize($(".destroyer"), (size * 0.178).toFixed(3));
		
		setTimeout(function () {
			var msg2FontSize = (size * 0.07).toFixed(0);
			var $board;
			if(transformed) {
				$board = $("#otherBoard");
			} else {
				$board = $("#myBoard");
			}
			var otherBoardOffset = $board.offset();
			var msg2Top = otherBoardOffset.top + $board.height() / 2 - Number(msg2FontSize) * 0.64;
			$("#msg2")
				.css("font-size", msg2FontSize + "px")
				.css("top", msg2Top);
				
			setTimeout(function () {
				var msg2Left = otherBoardOffset.left + $board.width() / 2 - $("#msg2").width() / 2 - Number(msg2FontSize) * 0.35;
				$("#msg2").css("left", msg2Left);
			}, 10);
		}, 10);
		
		if(transformed) {
			$("#myScreen")
				.css("transition-duration", "0s")			
				.css("transform", "perspective(" + 
				myScreenTransformDims.perspective + "px) translateY(" + 
				myScreenTransformDims.translateY + "px) translateZ(" + 
				myScreenTransformDims.translateZ + "px) rotate3d(1,0,0,70deg)");
			
			$(".otherScreen")
				.css("transition-duration", "0s")			
				.css("transform", "perspective(" + otherScreenTransformDims.perspective + 
					"px) translateY(0px) translateZ(0px) rotate3d(1,0,0,0deg)");					
		}
		
		BattleShip.resize(size);
	}
	
	function resetBoards() {
		transformed = false;
		$(".board table").remove();
		boards_created = false;
		$(".ship:not(.placeholder)").remove();
		$("#myPieces .ship").css("visibility", "visible");
		$("#otherPieces .ship").css("visibility", "hidden");
		enableEvents();
	}
	
	function enableEvents() {
		if(!eventsEnabled) {
			$(document.body).on("mousedown", ".ship", shipMouseDown);	
			$(document.body).on("mouseup", shipMouseUp);
			$(document.body).on("mousemove", mouseMove);		
			$("#btnGo").on("click", function () {
				startGame("5s");
			});
		}
		eventsEnabled = true;
	}
	
	function disableEvents() {
		if(eventsEnabled) {
			$("#btnGo").off("click", startGame);	
			$(document.body).off("mousedown", ".ship", shipMouseDown);	
			$(document.body).off("mouseup", shipMouseUp);
			$(document.body).off("mousemove", mouseMove);
		}
		eventsEnabled = false;
	}
	
	function createBoards() {
		if(!boards_created) {			
			//Create boards
			$(".board").each(function () {
				var $table = $("<table>");
				var $tr = $("<tr>");
				$tr.append("<th>&nbsp;</th>");
				
				//Create top row
				for(var i = 1; i <= boardSize; i++) {
					$tr.append("<th>" + i + "</th>");
				}
				$table.append($tr);				
				
				//Create body
				var labels = "ABCDEFGHIJ";
				for(var y = 0; y < boardSize; y++) {
					$tr = $("<tr>");
					for(var x = 0; x <= boardSize; x++) {					
						if(x === 0) {
							$tr.append("<th>" + labels.charAt(y) + "</th>");
						} else {
							$tr.append("<td class='cell cell_" + x + "-" + (y + 1) + "' data-x=" + x + " data-y=" + (y + 1) + " >&nbsp;</td>");
						}
					}
					$table.append($tr);
				}
				
				$(this).append($table);
			});
			boards_created = true;
		}
	}
	
	function setupGameBoard() {	
		scaleBoard();		
		$(window).on("resize", scaleBoard);		
		updateState();	
	}
	
	function shipMouseUp(e) {	
		if(ship_animating) {
			return;
		}
		//console.log("Mouse Up");
		//If a ship is being dragged to the board then place ship
		if($ship && $cellPlacement) {
			$placed_cells = $("#myBoard .cell-placing");
			$placed_cells.removeClass("cell-placing");
			$placed_cells.each(function () {					
				var $cell = $(this);
				var x = $cell.data("x");
				var y = $cell.data("y");
				myBoard[x - 1][y - 1].push($ship.data("ship"));
				setCellColor($cell);
			});
			$ship.data("placed-cells", $placed_cells);
			$ship.data("$cellPlacement", $cellPlacement);
			var offset = $cellPlacement.offset();
			var centerX = 0;
			var centerY = 0;				
			$ship.animate({
				left: offset.left + centerX,
				top: offset.top + centerY
			}, 250, function () {
                if($(this).find(".btn-rotate").length === 0) {
                    var $btn = $("<input class='btn2 btn-rotate' type='button' value='' />");	
                    $btn
                        .css("width", btnRotateDims.width)
                        .css("height", btnRotateDims.height)
                        .css("margin-left", btnRotateDims.marginLeft + "px")
                        .css("background-size", btnRotateDims.width + "px " + btnRotateDims.height + "px");
                    $btn.on("click", rotateShip);
                    $(this).append($btn);	                    
                    placeButton($(this));
                }
			});
			gameState.shipState[$selected_ship.data("shipname")] = {
				x: $cellPlacement.data("x"),
				y: $cellPlacement.data("y"),
				orientation: $ship.data("orientation")
			};
			$ship = false;
			$cellPlacement = false;					
			checkPlaced();	
			updateState();
		//No ship has been found then move ship back to original location.
		} else if($ship && $selected_ship[0] !== $ship[0]) {				
			var offset = $selected_ship.offset();				
			//console.log("Animation Start");
			$ship.animate({
				left: offset.left,
				top: offset.top
			}, 250, function () {
				//console.log("Animation End");
				$selected_ship.css("visibility", "visible");
				$ship.remove();
				ship_animating = false;
				$ship = false;
			});
			
			ship_animating = true;				
		}			
	}
	
	function shipMouseDown(e) {			
		if($(document.elementFromPoint(e.pageX, e.pageY)).hasClass("btn-rotate")) {		
			//rotateShip($(this));
			//return true;
		} else {
			startShipDrag(e, $(this));
		}				
	}
	
	function startShipDrag(e, $new_selected_ship) {
		if(ship_animating) {
			return;
		}
		
		//Set the selected ship global variable
		$selected_ship = $new_selected_ship;
		
		//Get the number of segments
		ship_data.segments = $selected_ship.data("segments");					
		
		//Clone the ship if it's being dragged from the original location
		if($selected_ship.hasClass("placeholder")) {			
			//Clone a new ship from the selected ship
			$ship = $selected_ship.clone();		
			$ship.data("ship", { name: $selected_ship.data("shipname"), segments: $selected_ship.data("segments"), hits: 0 });
			$ship.removeClass("placeholder");
			$selected_ship.css("visibility", "hidden");
		} else {				
			$ship = $selected_ship;
			
			//Remove the rotate button if it has one
			var $rotate_btn = $ship.find(".btn-rotate");
			if($rotate_btn.length > 0) {
				$rotate_btn.remove();
			}
			//Remove placed classes of new ships cells
			if($ship.data("placed-cells")) {
				removePlacedCells($ship);					
			}
		}
		
		//Set the default orientation to vertical if it isn't already set
		if(!$ship.data("orientation")) {
			$ship.data("orientation", "vertical");
		}
		
		//Calculate the drag offset for placing on the board
		var drag_segment = Math.floor((e.offsetY / $selected_ship.height()) * ship_data.segments);			
		var segment_centerY, segment_centerX;	
		
		if($ship.data("orientation") === "vertical") {
			var segment_height = $selected_ship.height() / ship_data.segments;
			segment_centerX = $selected_ship.width() / 2;
			segment_centerY = segment_height / 2;				
		} else {
			var segment_width = $selected_ship.width() / ship_data.segments;
			segment_centerX = segment_width / 2;
			segment_centerY = $selected_ship.height() / 2;
		}
		ship_data.offsetX = e.offsetX - segment_centerX;
		ship_data.offsetY = e.offsetY - segment_centerY;
		
		//Set the position of the dragging ship
		var offset = $selected_ship.offset();					
		$ship.css("position", "absolute");
		$ship.css("left", offset.left);
		$ship.css("top", offset.top);				
		
		$(document.body).append($ship);
		mouseMove(e);
	}
	
	function mouseMove(e) {
		//console.log("Mouse Move");
		if($ship) {
			var offset = $ship.offset();
			var dx = lx - e.pageX;
			var dy = ly - e.pageY;
			$ship.css("left", offset.left - dx);
			$ship.css("top", offset.top - dy);	
			
			//Create the ship shadow
			$cell = getCell(e.pageX - ship_data.offsetX, e.pageY - ship_data.offsetY);
			
			if($cell) {						
				$("#myBoard .cell-placing").removeClass("cell-placing");
				$cell.addClass("cell-placing");
				var cellX = Number($cell.data("x"));
				var cellY = Number($cell.data("y"));					
				if($ship.data("orientation") === "vertical") {
					for(var y = 1; y < ship_data.segments; y++) {
						var $nextCell = $("#myBoard .cell_" + cellX + "-" + (cellY + y));
						$nextCell.addClass("cell-placing");
					}
				} else {
					for(var x = 1; x < ship_data.segments; x++) {
						var $nextCell = $("#myBoard .cell_" + (cellX + x) + "-" + cellY);
						$nextCell.addClass("cell-placing");
					}
				}
				
				$cellPlacement = $cell;
			}
		}
		lx = e.pageX;
		ly = e.pageY;
	}
	
	function getShipIndex(boardCell, ship_data) {
		for(var i = 0; i < boardCell.length; i++) {
			if(ship_data.name === boardCell[i].name) {
				return i;
			}
		}
		return -1;
	}
	
	function removePlacedCells($ship) {
		$ship.data("placed-cells").each(function () {
			var $cell = $(this);
			var x = $cell.data("x");
			var y = $cell.data("y");	
			var ship_index = getShipIndex(myBoard[x - 1][y - 1], $ship.data("ship"));
			if(ship_index > -1) {
				myBoard[x - 1][y - 1].splice(ship_index, 1);
			}
			setCellColor($cell);
		});	
	}

	function getCell(x, y) {
		var $ships = $(".ship");
		$ships.hide();
		var $cell = $(document.elementFromPoint(x, y));
		$ships.show();
		if($cell.hasClass("cell")) {
			return $cell;
		}
		return false;
	}
	
	function setCellColor($cell) {
		var x = $cell.data("x");
		var y = $cell.data("y");									
		if(myBoard[x - 1][y - 1].length === 0) {
			$cell.removeClass("cell-placed");
			$cell.removeClass("cell-invalid");
		} else if(myBoard[x - 1][y - 1].length === 1) {
			$cell.addClass("cell-placed");
			$cell.removeClass("cell-invalid");
		} else {
			$cell.removeClass("cell-placed");
			$cell.addClass("cell-invalid");
		}
	}
	
	function rotateShip() {
		var $selected_ship = $(this).closest(".ship");
		var orientation = $selected_ship.data("orientation");
		var $first_cell = $selected_ship.data("placed-cells").first();
		removePlacedCells($selected_ship);
		var segments = $selected_ship.data("segments");
		var start_x = Number($first_cell.data("x"));
		var start_y = Number($first_cell.data("y"));
		var width = $selected_ship.width();
		var height = $selected_ship.height();
		
		var back_sizes_str = $selected_ship.css("background-size").trim();
		if(back_sizes_str.indexOf(" ") > -1) {
			var back_sizes = back_sizes_str.split(" ");
			var back_x = back_sizes[0];
			var back_y = back_sizes[1];
			$selected_ship.css("background-size", back_y + " " + back_x);			
		}
		
		//Swap the width and the height
		$selected_ship.css("width", height);
		$selected_ship.css("height", width);
		
		
		var $new_placed_cells = $first_cell;
		
		if(orientation === "vertical") {
			$selected_ship.css("background-image", "url(" + $selected_ship.data("shipname") + "_r.png)");				
			orientation = "horizontal";				
			var y = start_y;
			for(var x = start_x; x < start_x + segments; x++) {
				var $cell = $("#myBoard .cell_" + x + "-" + y);					
				if($cell.length > 0) {
					$new_placed_cells = $new_placed_cells.add($cell);
					myBoard[x - 1][y - 1].push($selected_ship.data("ship"));						
					setCellColor($cell);
				}
			}
		} else {
			$selected_ship.css("background-image", "url(" + $selected_ship.data("shipname") + ".png)");
			orientation = "vertical";
			var x = start_x;
			for(var y = start_y; y < start_y + segments; y++) {
				var $cell = $("#myBoard .cell_" + x + "-" + y);					
				if($cell.length > 0) {
					$new_placed_cells = $new_placed_cells.add($cell);
					myBoard[x - 1][y - 1].push($selected_ship.data("ship"));
					setCellColor($cell);
				}
			}
		}
		
		gameState.shipState[$selected_ship.data("shipname")] = {
			x: start_x,
			y: start_y,
			orientation: orientation
		};
		
		$selected_ship.data("orientation", orientation);
		$selected_ship.data("placed-cells", $new_placed_cells);
		placeButton($selected_ship);
		checkPlaced();
		updateState();
	}
	
	function placeButton($selected_ship) {
		var $btn = $selected_ship.find(".btn-rotate");
		if($selected_ship.data("orientation") === "vertical") {							
			$btn.css("margin-left", btnRotateDims.marginLeft + "px");
			$btn.css("margin-top", "0px");
		} else {
			$btn.css("margin-left", "0px");
			$btn.css("margin-top", btnRotateDims.marginLeft + "px");				
		}
	}
	
	function checkPlaced() {
		if($("#myBoard .cell-placed").length === 17) {
			$("#btnGo").prop("disabled", false);
		} else {
			$("#btnGo").prop("disabled", true);
		}
	}
	
	function startGame(transitionDuration) {
		$("#btnGo").prop("disabled", true);
		disableEvents();
		
		$(".btn-rotate").remove();
		$("#myBoard .cell-placed").removeClass("cell-placed");
		var $myScreen = $("#myScreen");
		var boardOffset = $myScreen.offset();
		$(".ship:not(.placeholder)").each(function () {
			var $ship = $(this);
			//$ship.data("$cellPlacement", false);
			var offset = $ship.offset();
			$ship.css("left", offset.left - boardOffset.left);
			$ship.css("top", offset.top - boardOffset.top);
			$myScreen.append($ship);
		});
		
		$myScreen.css("transition-duration", "5s");			
		$myScreen.css("transform", "perspective(" + 
			myScreenTransformDims.perspective + "px) translateY(" + 
			myScreenTransformDims.translateY + "px) translateZ(" + 
			myScreenTransformDims.translateZ + "px) rotate3d(1,0,0,70deg)");
		
		var $otherScreen = $(".otherScreen");
		$otherScreen.css("transition-duration", transitionDuration);
		$otherScreen
			.css("transform", "perspective(" + otherScreenTransformDims.perspective + "px) translateY(0px) translateZ(0px) rotate3d(1,0,0,0deg)");
		
		transformed = true;	
		BattleShip.begin(myBoard);				
	}
	
})(jQuery);


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
