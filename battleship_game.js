var BattleShip = (function ($) {
	var playerShots = [];	
	var playerSinks = [];
	var computerShots = [];
	var $cursor;
	var $targetCell;
	var $missile;
	var $shadowCell;
	var is_animating = false;
	var base_size = 1;
	var gameOverStatus = false;
	var eventsEnabled = false;
	
	function setupGamePlay(myBoard) {
		if(gameState.status === "setup") {
			$("#msg").css("transform", "rotateZ(0deg)");
					
			playerShots = [];	
			playerSinks = [];
			computerShots = [];
			gameState.playerShots = playerShots;
			gameState.playerSinks = playerSinks;
			gameState.computerShots = computerShots;
			gameState.status = "playing";
			updateState();	
			waitForAnimation(5000);			
			Server.begin(myBoard, beginTurn, getMove, gameOver, false);
		} else {
			playerShots = gameState.playerShots;
			playerSinks = gameState.playerSinks;
			computerShots = gameState.computerShots;
			status = "playing";
			for(var i = 0; i < playerShots.length; i++) {
				var $cell = $("#otherBoard .cell_" + playerShots[i].x + "-" + playerShots[i].y);
				if(playerShots[i].shot === "miss") {
					$cell.css("background-color", "white");
				} else {
					$cell.css("background-color", "red");
				}
			}
			for(var i = 0; i < playerSinks.length; i++) {
				$("#otherPieces ." + playerSinks[i]).css("visibility", "visible");
			}
			for(var i = 0; i < computerShots.length; i++) {
				var $cell = $("#myBoard .cell_" + computerShots[i].x + "-" + computerShots[i].y);
				if(computerShots[i].shot === "miss") {
					$cell.css("background-color", "white");
				} else {
					$cell.css("background-color", "red");
				}
			}
			
			Server.begin(myBoard, beginTurn, getMove, gameOver, true);
		}	
		$("#otherBoard .cell").addClass("cell-search");					
		setupCursor();
		setupMissile();		
		resize(base_size);
	}
	
	function gameOver(status) {
		gameOverStatus = status;
		setTimeout(function () {
			var message = "";
			if(status === "computer") {
				message = "YOU LOSE!";
			} else {
				message = "YOU WIN!";
			}
			var msgDims = getMsgDims(0.18 * base_size);
			//console.log(msgDims);
            $("#msg").html(message);
            $("#msg").css("display", "block");
            $("#msg").css("transform", "rotateZ(0deg)");
            $("#msg").css("left", "0");
            $("#msg").css("top", "-500px");

            setTimeout(function () {                
                $("#msg").css("transition-duration", "3s");
                setTimeout(function () {
                    $("#msg")
                        .css("font-size", msgDims.fontSize + "px")
                        .css("text-shadow", msgDims.txShadow)
                        .css("left", msgDims.left + "px")
                        .css("top", msgDims.top + "px")			
                        .css("transform", "rotateZ(1800deg)")
                        .css("opacity", "1");
                }, 10);    
            }, 10);
			
		}, 1000);
        
        gameState.gameOver = true;;
        updateState();
		//alert(status);
	}
	
	function waitForAnimation(t) {
		is_animating = true;
		setTimeout(function () {
			is_animating = false;
		}, t);
	}
	
	function getMove(shot) {
		if(!is_animating) {
			computerShots.push({shot: shot.shot, x: shot.x + 1, y: shot.y + 1});
			updateState();
			var $cell = $("#myBoard .cell_" + (shot.x + 1) + "-" + (shot.y + 1));			
			if(shot.shot === "hit") {
				//$cell.css("background-color", "red");				
				startMissileAnimation(shot.shot, false, $cell, 1.25);
			} else if(shot.shot === "miss") {				
				//$cell.css("background-color", "white");	
				startMissileAnimation(shot.shot, false, $cell, 1.25);
			}
		} else {
			setTimeout(function () {
				getMove(shot);			
			}, 100);
		}
	}
	
	function beginTurn() {
		if(!is_animating) {			
			setupEvents();
		} else {
			setTimeout(function () {
				beginTurn();			
			}, 100);
		}
	}
	
	function setupEvents() {
		if(!eventsEnabled) {
			$("#btnFire").on("click", fire);
			$("#otherBoard").on("mousemove", setShadow);	
			$("#otherBoard").on("mouseout", removeShadow);	
			$("#otherBoard").on("mousedown", ".cell", clickCell);	
		}
		eventsEnabled = true;
	}
	
	function disableEvents() {
		if(eventsEnabled) {
			$("#btnFire").off("click", fire);
			$("#otherBoard").off("mousemove", setShadow);	
			$("#otherBoard").off("mouseout", removeShadow);	
			$("#otherBoard").off("mousedown", ".cell", clickCell);
		}			
		$("#btnFire").prop("disabled", true);
		eventsEnabled = false;
	}
	
	function clickCell() {
		var $cell = $(this);		
		var offset = $cell.offset();
		$cursor.css("left", offset.left);
		$cursor.css("top", offset.top);
		$cursor.show();
		$("#btnFire").prop("disabled", false);
		$targetCell = $cell;
		$cell.addClass("cell-placing");		
	}
	
	function setupCursor() {
		//$cursor = $("<div class='cursor'></div>");
		$cursor = $(".cursor");
		var $sampleCell = $("#otherBoard .cell").first();
		var width = $sampleCell.width() + 3;
		var height = $sampleCell.height() + 3;
		
		$cursor.width(width);
		$cursor.height(height);
		$cursor.css("background-size", width + "px " + height + "px");
		$cursor.hide();
		$(document.body).append($cursor);
	}
	
	function setupMissile() {
		//$missile = $("<div class='missile'></div>");
		$missile = $(".missile");
		$missile.hide();
		$(document.body).append($missile);
	}
	
	function setShadow(e) {
		var $cell = $(document.elementFromPoint(e.pageX, e.pageY));
		
		if($cell.length > 0 && $cell.hasClass("cell") && $targetCell !== $cell) {
			if($shadowCell) {
				$shadowCell.removeClass("cell-placing");	
			}
			$cell.addClass("cell-placing");		
			$shadowCell = $cell;
		}
	}
	
	function removeShadow() {		
		if($shadowCell && (!$targetCell || $shadowCell[0] !== $targetCell[0])) {
			$shadowCell.removeClass("cell-placing");	
		}
	}
	
	function fire() {
		if($targetCell) {
			var x = $targetCell.data("x");
			var y = $targetCell.data("y");
			Server.endTurn(x - 1, y - 1, turnResults);
			disableEvents();
		}
	}
	
	function turnResults(data) {
		if(data.sunk) {
			playerSinks.push(data.sunk);
		}
		playerShots.push({
			shot: data.shot,
			x: $targetCell.data("x"),
			y: $targetCell.data("y")
		});
		updateState();
		startMissileAnimation(data.shot, data.sunk, $targetCell, 1);
	}
	
	var missileAnimated = false;
	var missileFrame = 0;	
	function startMissileAnimation(shot, sunk, $cell, missileScale) {
		var fromX;
		if(Math.random() > 0.5) {
			fromX = $(document.body).width();
		} else {
			fromX = -$missile.width();
		}
		var missileWidth = $missile.width() * missileScale;
		var missileHeight = $missile.height() * missileScale;
		
		var fromY = Math.round(Math.random() * $(document.body).height());
		var offset = $cell.offset();
		var x = Math.round(offset.left + ($cell.width() - missileWidth) / 2);
		var y = Math.round(offset.top + ($cell.height() - missileHeight) / 2);
		var dx = fromX - x;
		var dy = fromY - y;
		var radAngle = Math.atan2(dy, dx);
		var angle = Math.round(radAngle * 180 / Math.PI) - 90;
		var finalX = x + Math.round(Math.cos(radAngle) * missileHeight / 6);
		var finalY = y + Math.round(Math.sin(radAngle) * missileHeight / 6);
		$missile.show();
		/*
		$missile.css("transform", "perspective(1500px) translate3d(" + finalX + "px, " + finalY + "px, 0) rotate3d(0,0,1," + angle + "deg)");
		var strAnimation = "<style>@keyframes animateMissile {" +
				"from { " +
					"transform: perspective(1500px) translate3d(" + fromX + "px, " + fromY + "px, 0) rotate3d(0,0,1," + angle + "deg);" +
				"}" + 
				"to { " +
					"transform: perspective(1500px) translate3d(" + finalX + "px, " + finalY + "px, 0) rotate3d(0,0,1," + angle + "deg);" +
				"}" +
			"}</style>";
		$("#dynamicStyles").html(strAnimation);		
		$missile.css("animation-duration", "1s");
		$missile.css("animation-name", "animateMissile");		*/
		$missile.css("transition-duration", "0s");
		$missile.css("transform", "perspective(1500px) translate3d(" + fromX + "px, " + fromY + "px, 0) rotate3d(0,0,1," + angle + "deg)");
		setTimeout(function () {	
			$missile.css("transition-duration", "0.9s");
			$missile.css("transform", "perspective(1500px) translate3d(" + finalX + "px, " + finalY + "px, 0) rotate3d(0,0,1," + angle + "deg)");
		}, 100);
		
		missileAnimated = true;
		setTimeout(function () {
			missileAnimated = false;	
			if(shot === "hit") {
				$cell.css("background-color", "red");		
				if(sunk) {
					//alert("You sunk a " + sunk);
					$("#otherPieces ." + sunk).css("visibility", "visible");
				}
			} else if(shot === "miss") {
				$cell.css("background-color", "white");
			}	
		}, 1000);
		animateMissile();
	}
	
	function animateMissile() {
		if(missileAnimated) {	
			var x = (missileFrame * (base_size * 0.048)).toFixed(3);
			$missile.css('background-position', x + "px");
			missileFrame++;
			if(missileFrame > 3) {
				missileFrame = 0;
			}
			setTimeout(animateMissile, 120);
		} else {
			$missile.hide();
		}
	}
	
	function resize(size) {
		base_size = size;
		$cursor = $(".cursor");
		if($cursor.length > 0) {			
			var $sampleCell = $("#otherBoard .cell").first();
			$cursor.width($sampleCell.width() + 3);
			$cursor.height($sampleCell.height() + 3);			
			$cursor.css("background-size", ($sampleCell.width() + 3) + "px " + ($sampleCell.height() + 3) + "px");
			if($targetCell) {
				var offset = $targetCell.offset();
				$cursor.css("left", offset.left);
				$cursor.css("top", offset.top);
			}
		}
		if($missile) {
			var missileWidth = (size * 0.048);
			var missileHeight = (size * 0.14);
			$missile
				.css("width", missileWidth.toFixed(3) + "px")
				.css("height", missileHeight.toFixed(3) + "px")
				.css("background-size", (missileWidth * 3).toFixed(3) + "px " + missileHeight.toFixed(3) + "px");
		}
		
		var msgDims;
		if(gameOverStatus) { 
			msgDims = getMsgDims(0.18 * size);
			
		} else {
			msgDims = getMsgDims(0.5 * size);
		}
		
		$("#msg")
			.css("transition-duration", "0s")
			.css("font-size", msgDims.fontSize + "px")
			.css("text-shadow", msgDims.txShadow)
			.css("left", msgDims.left + "px")
			.css("top", msgDims.top + "px");
	}
	
	function getMsgDims(fontBaseSize) {		
		var len = $("#msg").text().length;
		var dims = {};
		dims.fontSize = (fontBaseSize).toFixed(0);			
		dims.txtShadow = "2px 2px 8px black";
		if(+dims.fontSize < 45) { 
			dims.txtShadow = "1px 1px 4px black";
		}
		
		var $myBoard = $("#otherBoard");
		var myBoardOffset = $myBoard.offset();
		var msgOffset = +dims.fontSize / 10;
		if(len >= 8) {
			msgOffset = - +dims.fontSize / 10;
		}
		dims.left = (myBoardOffset.left + msgOffset).toFixed(3);
		dims.top = (myBoardOffset.top + $myBoard.height() / 2 - +dims.fontSize / 2).toFixed(3);
		return dims;
	}
	
	return {
		begin: setupGamePlay,
		resize: resize,
		disableEvents: disableEvents
	};
})(jQuery);