var SinglePlayerServer = (function ($) {
	var playerBoard;
	var computerBoard;
	var ships;
	var ships2;
	var playerBeginTurn;
	var playerGetMove;
	var gameOverCallback;
	var sunk;
	var gameOver;
	var leads;
	
    function initVariables() {
        ships = [
            { name: "carrier", segments: 5, hits: 0 },
            { name: "battleship", segments: 4, hits: 0 },
            { name: "cruiser", segments: 3, hits: 0 },
            { name: "submarine", segments: 3, hits: 0 },
            { name: "destroyer", segments: 2, hits: 0 }	
        ];
        ships2 = {
            "carrier": { name: "carrier", segments: 5, hits: 0 },
            "battleship": { name: "battleship", segments: 4, hits: 0 },
            "cruiser": { name: "cruiser", segments: 3, hits: 0 },
            "submarine": { name: "submarine", segments: 3, hits: 0 },
            "destroyer": { name: "destroyer", segments: 2, hits: 0 }	
        };
        
        sunk = {
            player: 0,
            computer: 0
        };
        gameOver = false;
        leads = [];
    }
    
	function serverSetup(loadGame) {
		if(loadGame) {
			ships = gameState.ships;
			computerBoard = gameState.computerBoard;
			leads = gameState.leads;
			sunk = gameState.sunk;
		} else {
			gameState.leads = leads;
			gameState.ships = ships;
			gameState.sunk = sunk;
			computerBoard = [];
			
			for(var x = 0; x < boardSize; x++) {				
				computerBoard.push([]);
				for(var y = 0; y < boardSize; y++) {
					computerBoard[x].push(0);
				}
			}		
			for(var i = 0; i < ships.length; i++) {
				var ship = ships[i];
				var valid_place = false;
				var attempts = 100;
				while(!valid_place && attempts-- >= 0) {
					var dx = 0;
					var dy = 0;
					var isVertical = Math.random() > 0.5;
					var x = Math.floor(Math.random() * computerBoard.length);
					var y = Math.floor(Math.random() * computerBoard[0].length);
					if(isVertical) {
						dy = 1;
					} else {
						dx = 1;
					}
					//Make a copy of the board to undo changes if it's invalid
					var tempComputerBoard = JSON.parse(JSON.stringify(computerBoard));
					for(var s = 0; s < ship.segments; s++) {
						if(x < computerBoard.length && y < computerBoard[0].length && computerBoard[x][y] === 0) {						
							//console.log(x);
							//console.log(y);
							//console.log(computerBoard[x]);
							computerBoard[x][y] = { shipIndex: i, hit: false };						
							if(s === ship.segments - 1) {
								valid_place = true;
							}
						} else {
							break;
						}
						x += dx;
						y += dy;	
					}
					if(!valid_place) {
						//Rewind board placement
						computerBoard = JSON.parse(JSON.stringify(tempComputerBoard));
					}
				}
				//console.log(attempts);
				//console.log(computerBoard);
			}
			//console.log(computerBoard);			
		}
		gameState.computerBoard = computerBoard;
		//logBoard();
		updateState();
	}
	
	function logBoard() {
		var shipLog = "";
		for(var y = 0; y < boardSize; y++) {							
			for(var x = 0; x < boardSize; x++) {
				if(computerBoard[x][y] === 0) {
					shipLog += 0 + " ";
				} else {
					shipLog += (computerBoard[x][y].shipIndex + 1) + " ";
				}
			}
			shipLog += "\n";
		}	
		console.log("*******************");
		console.log(shipLog);
		console.log("*******************");
	}
	
	function checkBoard(x, y, callback, errorCallback) {
		if(computerBoard[x][y] === 0) {
			callback({shot: "miss"});
			//setTimeout(shootAtPlayer, 1100);
			shootAtPlayer();
		} else {			
			if(computerBoard[x][y].hit) {
				errorCallback();
				return;
			} else {
				var ship = ships[computerBoard[x][y].shipIndex];
				computerBoard[x][y].hit = true;
				ship.hits++;
				if(ship.hits === ship.segments) {
					callback({shot: "hit", sunk: ship.name});
					sunk.computer++;
					gameOver = checkWin();
					if(gameOver) {
						gameOverCallback(gameOver);
					}
				} else {
					callback({shot: "hit"});
				}
				if(!gameOver) {
					setTimeout(function () { playerBeginTurn(false); }, 1100);
				}
			}
		}
		
		updateState();
	}
	
	function shootAtPlayer() {
		var has_shot = true;
		var shot;
		while(has_shot) {
			var x, y;
			if(leads.length > 0) {
				var lead = leads[Math.floor(Math.random() * leads.length)];
				x = Number(lead.name.split("_")[0]);
				y = Number(lead.name.split("_")[1]);
				var chance = Math.random();
				if(chance <= 0.25) {
					x++;
				} else if(chance <= 0.5) {
					x--;
				} else if(chance <= 0.75) {
					y++;
				} else {
					y--;
				}
				if(x < 0 || x > playerBoard.length - 1 || y < 0 || y > playerBoard[0].length - 1) {
					continue;
				}
			} else {
				x = Math.floor(Math.random() * playerBoard.length);
				y = Math.floor(Math.random() * playerBoard[0].length);
			}
			if(playerBoard[x][y] === 0) {
				shot = {shot: "miss", x: x, y: y, sunk: false};
				has_shot = false;
				playerBoard[x][y] = 1;
			} else if(playerBoard[x][y] !== 1 && !playerBoard[x][y].hit) {
				has_shot = false;
				playerBoard[x][y].hit = true;
				ships2[playerBoard[x][y].ship].hits++;
				shot = {shot: "hit", x: x, y: y, sunk: false};
				var new_lead = {
					name: x + "_" + y,
					ship_name: playerBoard[x][y].ship
				};
				if(ships2[playerBoard[x][y].ship].hits === ships2[playerBoard[x][y].ship].segments) { 
					sunk.player++;
					shot.sunk = playerBoard[x][y].ship;
					for(var i = leads.length - 1; i >= 0; i--) {
						if(leads[i].ship_name === new_lead.ship_name) {
							leads.splice(i, 1);
						}
					}
					gameOver = checkWin();
					if(gameOver) {
						gameOverCallback(gameOver);
					}
				} else {
					leads.push(new_lead);
				}
			}
		}
		
		setTimeout(function () {
			playerGetMove(shot);
			if(!gameOver) {
				if(shot.shot === "miss") {			
					playerBeginTurn();
				} else {
					shootAtPlayer();
				}
			}
		}, 1100);	

		updateState();
	}
		
	function checkWin() {
		if(sunk.player === ships.length) {
			return "computer";
		}
		if(sunk.computer === ships.length) {
			return "player";
		}
		
		return false;
	}
	
	function setupPlayerBoard(myBoard, loadGame) {
		if(loadGame) {
			ships2 = gameState.ships2;
			playerBoard = gameState.playerBoard;
		} else {
            initVariables();
			playerBoard = [];
			
			gameState.ships2 = ships2;
			gameState.playerBoard = playerBoard;			
			
			for(var x = 0; x < boardSize; x++) {				
				playerBoard.push([]);
				for(var y = 0; y < boardSize; y++) {				
					playerBoard[x].push(0);
				}
			}	
			for(var x = 0; x < boardSize; x++) {
				for(var y = 0; y < boardSize; y++) {				
					//myBoard[x][y] = {hit: false, ship: myBoard[x][y][0]};
					if(myBoard[x][y].length > 0) {
						playerBoard[x][y] = {hit: false, ship: myBoard[x][y][0].name};
					}
				}
			}
		}
	}
	
	return {
		begin: function (myBoard, myBeginTurn, myGetMove, gameOver, loadGame) {
			//playerBoard = myBoard;
			setupPlayerBoard(myBoard, loadGame);
			playerBeginTurn = myBeginTurn;
			playerGetMove = myGetMove;
			gameOverCallback = gameOver;
			serverSetup(loadGame);
			playerBeginTurn();
		},
		endTurn: function (x, y, callback) {
			checkBoard(x, y, callback);
		},
		stop: function () {
		}
	};
})(jQuery);