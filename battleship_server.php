<?php 
	
	require('battleship_config.php');
	
	if(isset($_POST['operation'])) {
		$op = $_POST['operation'];
		if($op === 'create') {
			create();
		} elseif($op === 'begin') {
			begin();
		}  elseif ($op === 'load') {
			load();
		} elseif ($op === 'get_move') {
			get_move();
		} elseif ($op === 'end_turn') {
			end_turn();
		}
	}
	
	function create() {
		$game = array();	
		$game['id'] = 0;
		$game['players'] = array();		
		$game['started'] = time();
		$game['updated'] = time();
		$game['setup'] = 0;
		
		array_push($game['players'], createPlayer());
		array_push($game['players'], createPlayer());
		
		$game['players'][0]['turn'] = true;
		
		save_game($game);
		
		$data = new stdClass;
		$data->id = $game['id'];
		$data->playerId = $game['players'][0]['id'];
		
		//print_r($game);
		send_json($data);
	}
	
	function createPlayer() {
		$player = array();
		$player['id'] = uniqid('P');
		$player['board'] = array();		
		for($x = 0; $x < 10; $x++) {
			$player['board'][] = array();
			for($y = 0; $y < 10; $y++) {
				$player['board'][$x][] = 0;
			}
		}
		$player['ships'] = array();
		$player['ships']['carrier'] = createShip('carrier', 5);
		$player['ships']['battleship'] = createShip('battleship', 4);
		$player['ships']['cruiser'] = createShip('cruiser', 3);
		$player['ships']['submarine'] = createShip('submarine', 3);
		$player['ships']['destroyer'] = createShip('destroyer', 2);
		
		$player['sunk'] = array();
		$player['move'] = false;
		$player['turn'] = false;
		
		return $player;
	}
	
	function createShip($name, $segments) {
		$ship = array();
		$ship['hits'] = 0;
		$ship['name'] = $name;
		$ship['segments'] = $segments;		
		return $ship;
	}
	
	function get_games() {
		if(!file_exists($GLOBALS['gamesFile'])) {
			file_put_contents($GLOBALS['gamesFile'], json_encode(array()), LOCK_EX);
		}
		return json_decode(file_get_contents($GLOBALS['gamesFile']), true);		
	}
	
	function save_game(&$game) {
		$games = get_games();
		if($game['id'] === 0) {
			$game['id'] = create_game_id($games);
		}
		if(array_key_exists($game['id'], $games)) { 
			$gameFileName = $games[$game['id']];
		} else {
			$gameFileName = uniqid('GF') . '.json';
			$games[$game['id']] = $gameFileName;
			ksort($games);
			file_put_contents($GLOBALS['gamesFile'], json_encode($games), LOCK_EX);
		}
		$game['updated'] = time();
		
		file_put_contents($GLOBALS['dataRoot'] . $gameFileName, json_encode($game), LOCK_EX);
		
		//echo 'game_id is: ' . $game['id'];
	}
	
	function create_game_id($games) {
		//echo 'creating game id';
		$new_id = 1;
		foreach($games as $key => $value) {				
			if($new_id < $key) {
				break;
			}
			$new_id++;
		}
		
		//echo "game id is $new_id";
		return $new_id;
	}
	
	function get_game($game_id) {
		$games = get_games();
		foreach($games as $id => $gameFileName) {				
			//echo "game_id = $game_id; id = $id; file = $gameFileName\n";
			if($game_id == $id) {				
				//echo 'true';
				$path = $GLOBALS['dataRoot'] . $gameFileName;
				return json_decode(file_get_contents($path), true);	
			} else {
				//echo 'false';
			}
		}
		echo 'Failed to find game';
		return false;
	}
	
	function &get_player(&$game, $player_id) {
		foreach($game['players'] as $key => $player) {
			if($player_id === $player['id']) {
				return $game['players'][$key];
			}
		}
		return false;
	}
	
	function &get_other_player(&$game, $player_id) {
		$other_player = false;
		$player_found = false;
		foreach($game['players'] as $key => $player) {
			if($player_id === $player['id']) {
				$player_found = true;
			} else {
				$other_player = $key;
			}
		}
		if($player_found) {
			return $game['players'][$other_player];
		} else {
			return false;
		}
	}
	
	function begin() {
		if(isset($_POST['board']) && isset($_POST['gameRoom']) && isset($_POST['playerId'])) {
			$board = $_POST['board'];
			$game_id = $_POST['gameRoom'];
			$player_id = $_POST['playerId'];
			$game = get_game($game_id);
			if($game) {
				$player = &get_player($game, $player_id);
				if($player) {
					$segments = 0;
					foreach($board as $x => $row) {
						foreach($row as $y => $cell) {
							$player['board'][$x][$y] = $cell[0]['name'];
							$segments++;
						}
					}					
					$game['setup']++;
					save_game($game);
					if($segments === 17) {						
						send_json('success');
					} else {
						echo 'Invalid # of segments $segments';
					}
				} else {
					echo "Player: $player_id not found.";
				}
			} else {
				echo "Game: $game_id not found.";
			}			
		} else {
			echo 'Invalid request';
			print_r($_POST);
		}
	}
	
	function load() {
		//Gets the other players id #
		if(isset($_POST['gameRoom'])) {
			$game_id = $_POST['gameRoom'];
			$game = get_game($game_id);
			if($game) {
				$player_id = $game['players'][1]['id'];
				send_json($player_id);
			} else {
				echo 'Failed to load game.';
			}
		} else {
			echo 'Inavlid request';
		}
	}
	
	function get_move() {
		if(isset($_POST['gameRoom']) && isset($_POST['playerId'])) {
			$game_id = $_POST['gameRoom'];
			$playerId = $_POST['playerId'];
			$game = get_game($game_id);
			if($game) {
				
				$player = &get_player($game, $playerId);				
				if($player) {
					$res_data = new stdClass;
					$res_data->shot = $player['move'];
					$res_data->turn = $player['turn'];
					
					$win = check_win($game);
					if($win) {						
						$res_data->game_over = $win;
					} else { 
						$res_data->game_over = false;
					}
					
					$player['move'] = false;					
					save_game($game);
					
					//Make sure both players finish setup before starting first turn.
					if($game['setup'] < 2) {
						$res_data->turn = false;
					}
					send_json($res_data);					
				} else {
					echo 'Failed to get player';
				}								
			}
		} else {
			echo 'Invalid request';
			print_r($_POST);
		}
	}
	
	function check_win($game) {		
		foreach($game['players'] as $player) {
			$sunk_count = 0;
			foreach($player['ships'] as $ship) {
				if($ship['segments'] === $ship['hits']) {
					$sunk_count++;
				}
			}
			if($sunk_count === 5) {
				return $player['id'];
			}
		}
		
		return false;
	}
	
	function end_turn() {
		if(isset($_POST['gameRoom']) && isset($_POST['playerId']) && isset($_POST['x']) && isset($_POST['y'])) {
			$game_id = $_POST['gameRoom'];
			$playerId = $_POST['playerId'];
			$x = $_POST['x'];
			$y = $_POST['y'];
			
			$game = get_game($game_id);
			if($game) {
				$player = &get_player($game, $playerId);
				$other_player = &get_other_player($game, $playerId);
				if($other_player && $player) {		
					$res_data = new stdClass;
					$res_data->sunk = false;
					
					$move = new stdClass;
					$move->name = 'shot';
					$move->x = $x;
					$move->y = $y;					
					if($other_player['board'][$x][$y] === 0) {						
						//Miss
						$other_player['board'][$x][$y] = 1;
						$res_data->shot = 'miss';
						
						$move->shot = 'miss';		

						$other_player['move'] = $move;						
						$player['turn'] = false;
						$other_player['turn'] = true;
					} elseif(isset($other_player['ships'][$other_player['board'][$x][$y]])) {						
						//hit						
						$ship = &$other_player['ships'][$other_player['board'][$x][$y]];						
						$ship['hits']++;
						$other_player['board'][$x][$y] = 2;
						
						$move->shot = 'hit';
						
						$res_data->shot = 'hit';
						//echo $ship['hits'] . '\n';
						//print_r($other_player);
						
						if($ship['hits'] === $ship['segments']) {
							$res_data->sunk = $ship['name'];
							$move->sunk = $ship['name'];
						}
						
						$other_player['move'] = $move;
						$player['turn'] = true;
						$other_player['turn'] = false;
					} else {						
						//Invalid shot
						$res_data = 'invalid shot';
						
						$player['turn'] = true;
						$other_player['turn'] = false;										
					}
										
					//If the game is over then nobody has a turn
					if(check_win($game)) {						
						$player['turn'] = false;
						$other_player['turn'] = false;	
					}
					
					save_game($game);					
					send_json($res_data);
					
				} else {
					echo 'Cannot find other player';
				}
			} else {
				echo 'Cannot find game';
			} 
		} else {
			echo 'Invalid request\n';
			print_r($_POST);
		}
	}
	
	function send_json($data) {
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
?>